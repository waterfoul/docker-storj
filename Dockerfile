FROM node:4.4.5-wheezy
MAINTAINER Aaron Aichlmayr <waterfoul@gmail.com>

VOLUME ["/shareData"]

RUN npm install --quiet -g storjshare-cli

CMD ["storjshare", "start", "--datadir", "/shareData"]